
operators = {
    'equals': lambda x,y: x == y,
    'different': lambda x,y: x != y,
    'in': lambda e, lst: e in lst,
    'empty': lambda x: x == [] or x == {},
    'false': lambda x: x is False,
    'true': lambda x: x is True,
}