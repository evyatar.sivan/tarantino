from parsers.queryFileParser import QueryFileParser
from parsers.validatorFileParser import ValidatorFileParser
from operators import operators
from config import config
from logger import log
import requests
import time
import json

PATH_DELIMITER = '.'

class TesterException(Exception):
    pass

class Tester:

    def __init__(self, queriesFile):
        self.queriesFile = queriesFile
        try:
            self.queryFileParser = QueryFileParser(queriesFile)
        except:
            raise TesterException("Error occured while reading query file: {}. check its structure and syntax")
        try:
            # Assuming queryFile and validatorFile have identical names in different paths
            self.validatorFileParser = ValidatorFileParser(queriesFile) 
        except:
            raise TesterException("Error occured while reading validator file: {}. check its structure and syntax")

    def _addMockQuery(self):
        headers = {
            'Content-Type': 'application/json'
        }
        data = self.queryFileParser.getContent()
        try:
            resp = requests.post(config.MOCK_URL, json=data, headers=headers)
            return resp
        except Exception as e:
            raise TesterException("Network error. check that the mock service is up and running, also validate the url")

    def _queryRepository(self):
        data = self.validatorFileParser.getContent()['query']
        headers = self.validatorFileParser.getContent()['headers']
        headers = { k:str(v) for k,v in headers.items()}
        try:
            resp = requests.post(config.REPOSITORY_URL, json=data, headers=headers)
            return resp.json()
        except Exception as e:
            raise TesterException("Network error. check that the repository service is up and running, also validate the url")

    def _validate(self, value, validator):
        operator_name = validator['operator']
        try:
            operator = operators[operator_name]
        except:
            raise TesterException("Invalid operator specified. valid values: {}. got: {}".format(list(operators.keys(), operator_name)))
        return operator(value, validator['expectedValue'])

    def _getValue(self, object, index):
        try:
            if isinstance(object, dict):
                return object[index]
            if isinstance(object, list):
                return object[int(index)]
            else:
                raise TesterException("noniterable object: {} has no item for index -{}-".format(object, index))
        except:
            raise TesterException("object: {} has no item for index {}".format(object, index))

    
    def _getValueByPath(self, data, path):
        if path == []:
            return data
        next = path.pop(0)
        nextData = self._getValue(data, next)
        return self._getValueByPath(nextData, path)     

    def _runValidations(self):
        failed = False 
        repositoryResponse = self._queryRepository()
        print (repositoryResponse)
        validators = self.validatorFileParser.getContent()['validators']
        for validator in validators:
            try:
                value = self._getValueByPath(repositoryResponse, validator['path'].split(PATH_DELIMITER))
                if not self._validate(value, validator):
                    self._fail(value, validator)
                    failed = True
                elif not failed:
                    self._pass(validator)
            except:
                self.__log("\u2716 Error in validator: {} - Could not get value with path: {}.".format(validator["name"], validator['path']))
                failed = True
        if not failed: 
            self._succeed()
        return failed

    def _pass(self, validator):
        self.__log("\u2714 Test passed for validator: {}".format(validator["name"]))

    def _fail(self, value, validator):
        self.__log("\u2716  Error. Test Failed in File: {}. \n\t\t value path: {}, \n\t\t Validation- Value: {} not {} to Expected value:{}".format(
                self.queriesFile, validator['path'], value, validator['operator'], validator['expectedValue'])
            )

    def __log(self, text):
        log("\t{}".format(text))

    def _succeed(self, ):
        self.__log("All Validators Succeeded for file: {}.".format(self.queriesFile))

    def run(self):
        log("Running Test for file: {}".format(self.queriesFile))
        addQueryRepsonse = self._addMockQuery()
        if not addQueryRepsonse.ok:
            raise TesterException("Failed to add query to mock. got repsonse status: {}".format(addQueryRepsonse.status_code))
        time.sleep(config.WAITING_TIME_SEC)
        return self._runValidations()