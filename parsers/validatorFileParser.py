import json
from config import config
from jsonschema import validate as schemaValidate

class InvalidJSONException(Exception):
    pass

SCHEMA = {
    "type": "object",
    "properties": {
        "query": {
            "type": "object",
            "properties": {
                "body": {
                    "type": "object",
                    "properties": {}
                },
                "headers": {
                    "type": "object",
                    "properties": {}
                }
            }
        },
        "validators": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {"type": "string"},
                    "path": {"type": "string"},
                    "expectedValue": {},
                    "operator": {"type": "string"},
                }
            }
        }
    }
}

class ValidatorFileParser():

    def __init__(self, validatorFilePath):
        self.validatorFilePath = '/'.join([config.VALIDATORS_PATH, validatorFilePath])
        self._content = None
        self._load()

    def _load(self):
        with open(self.validatorFilePath, "r",  encoding='utf-8') as f:
            try:
                self._content = json.load(f, encoding='utf-8')
            except:
                raise InvalidJSONException("Failed to load json file: " + self.validatorFilePath )
        self.validateFileStructure()

    def validateFileStructure(self):
        schemaValidate(instance=self._content,schema=SCHEMA)


    def getContent(self):
        return self._content
