import json
from config import config
from jsonschema import validate as schemaValidate

class InvalidJSONException(Exception):
    pass

SCHEMA = {
    "type": "object",
    "properties": {
        "query": {
            "type": "object",
            "properties": {
                "query": {
                    "type": "string"
                },
                "extensions": {
                    "type": "object",
                    "properties": {}
                }
            }
        },
        "responses":{
            "type": "array",
            "items":{
                "type": "object",
                "properties":{
                    "body": {
                        "type": "object",
                        "properties": {}
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        },
        "onEmptyResponse":{
            "type": "object",
            "properties":{}
        }
    }
}

class QueryFileParser():

    def __init__(self, queryFilePath):
        self.queryFilePath = '/'.join([config.QUERIES_PATH, queryFilePath])
        self._content = None
        self._load()

    def _load(self):
        with open(self.queryFilePath, "r", encoding='utf-8') as f:
            try:
                self._content = json.load(f, encoding='utf-8')
            except: 
                raise InvalidJSONException("Failed to load json file: " + self.queryFilePath )
        self.validateFileStructure()

    def validateFileStructure(self):
        schemaValidate(instance=self._content,schema=SCHEMA)

    def getContent(self):
        return self._content
