from config import config
from tester import Tester, TesterException
from logger import log
import os, sys
import re

def getQueriesFiles():
    file_list = os.listdir(config.QUERIES_PATH)
    regex = re.compile(config.FILE_REGEX)
    return filter(regex.search, file_list)

def logResultMessage(failed):
    message = "\nDone Running tests. result: {}"
    if failed:
        result = "Failed"
    else:
        result = "Success!"
    log(message.format(result))

def exitWithStatusCode(failed):
    if failed:
        statusCode = 1
    else:
        statusCode = 0
    sys.exit(statusCode)
    
def main():
    failed = False
    queriesFiles = getQueriesFiles()
    for queriesFile in queriesFiles:
        try:
            tester = Tester(queriesFile)
            failed = tester.run() or failed
        except TesterException as te:
            print("\t\u2716 Error: {}. skipping test...{}".format(te.args[0], queriesFile))
            failed = True
    logResultMessage(failed)
    exitWithStatusCode(failed)
    

if __name__ == '__main__':
    main()