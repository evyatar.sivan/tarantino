
# End 2 End Testing Script - python

### This project is a python tool to run end2end test scripts for hermes and repositories

## Set up the environment

Install python 3.7

add python and pip to PATH 

clone the project

get into the project directory

run: 
```
python -m venv venv
```
this will create a directory named "venv"

run:
```
.\venv\Scripts\activate 
```
download this [pip.ini](https://confluence.app.iaf/download/attachments/82683563/pip.conf?version=1&modificationDate=1552229669000&api=v2) file and put it inside the venv directory
run:
```
pip install -r requirements.txt
```
## How To Use:
In this project there are two directories named "testCases/mockQueries" and "testCases/repositoryValidation".

In __"testCases/mockQueries"__ directory we put json files that describe the responses that the mock service should return.

for more explanation about the mock refer to [it's bitbucket page](https://bitbucket.app.iaf/projects/G2E/repos/http-testing-mock/browse).

the files should be in the next structure:
```
{
    "query": {
        ... graphql query json ...
    },
    "responses": [
        { ... response object #1 ...},
        { ... response object #2 ...},
        ...
    ],
    _optional field:
    "onEmptyResponse": {
        a json object specifies what to return if there are no other responses in the response queue
    }
}
```

In __"testCases/repositoryValidation"__ directory we put json files that describe the validations to make upon the repository.

the files should be in the next structure:
```
{
    "query": {
        "a json object who represents the query to the repository"
    },
    "validators":[
        {
            "name": "a description for the validator",
            "path": "a.path.to.the.desired.value",
            "expectedValue": "what you expect the value to be",
            "operator": "the relation between the actual value and the expected value"
        },
        { ... validator #2 ...}
    ]
}
```

### Please Notice! the query file and the validation file must have the __same name__ in order to work !!!