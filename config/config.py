import os

MOCK_URL = os.environ.get('MOCK_URL', 'http://localhost:3000/createQuery')
REPOSITORY_URL = os.environ.get('REPOSITORY_URL', 'http://localhost:8088/v1/graphql')

QUERIES_PATH = "testCases/mockQueries"
VALIDATORS_PATH = "testCases/repositoryValidators"

WAITING_TIME_SEC = os.environ.get('WAITING_TIME_SEC', 5)
FILE_REGEX = os.environ.get('FILE_REGEX', '')