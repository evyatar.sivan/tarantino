import os
import logging
import logstash

log_host = os.environ.get('LOG_HOST')
log_port = os.environ.get('LOG_PORT')

logger = logging.getLogger('tarantino-logger')

if not log_host or not log_port:
    handler = logging.StreamHandler()
else:
    handler = logstash.logstashHandler(log_host, log_port)
 
logger.setLevel(logging.INFO)

logger.addHandler(handler)

def log(message):
    logger.info(message)